#ifndef _OBJECT_H_
#define _OBJECT_H_

#include <glm/glm.hpp>


class Ray;

class Object
{
public:
    Object() = default;
    virtual ~Object() {}

    virtual bool intersect(const Ray& ray, float& dist) = 0;
    virtual void computeColorNormal(const Ray& ray, float dist,
                                    glm::vec3& color, glm::vec3& normal) = 0;
};


#endif /* _OBJECT_H_ */
