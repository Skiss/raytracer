#ifndef _RAYTRACER_H_
#define _RAYTRACER_H_

#include <fstream>
#include <glm/glm.hpp>


class Scene;
class Ray;

class RayTracer
{
public:
    RayTracer(unsigned pixelWidth, unsigned pixelHeight, float width, float height, float depth);
    ~RayTracer() =default;

    void setCamera(float width, float height, const glm::vec3& origin, const glm::vec3& direction);
    void setResolution(unsigned pixelWidth, unsigned pixelHeight);
    void setScene(Scene* scene) { scene_ = scene; }

    void render();
    void computeColor(const Ray& ray, glm::vec3& color);

private:
    glm::vec3 origin_;
    glm::vec3 direction_;

    float width_;
    float height_;
    unsigned pixelW_;
    unsigned pixelH_;
    float depth_;

    float precompW_;
    float precompH_;

    Scene* scene_;

    std::ofstream output;

    void generateRay(unsigned x, unsigned y, Ray& ray);
};


#endif /* _RAYTRACER_H_ */
