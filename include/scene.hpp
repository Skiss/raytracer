#ifndef _SCENE_H_
#define _SCENE_H_

#include <glm/glm.hpp>

#include <iostream>
#include <vector>
#include <string>
#include <fstream>

#include "material.hpp"
#include "objects.hpp"


inline
std::ostream& operator<<(std::ostream& out, const glm::vec3& v)
{
    out << v.x << "," << v.y << "," << v.z;
    return out;
}

struct Light
{
    glm::vec3 position;
    glm::vec3 color;
};

class Scene
{
public:
    Scene(int width, int height, const std::string& file);
    ~Scene() = default;

    void addObject(Object* o) { objects.push_back(o); }
    void addLight(const Light& l) { lights.push_back(l); }
    void addAmbientLight(const glm::vec3& ambient) { ambientLight = ambient; }

    void render();

private:
    unsigned height_;
    unsigned width_;

    std::vector<Object*> objects;
    std::vector<Light> lights;
    glm::vec3 ambientLight;
    std::ifstream sceneFile;
    std::ofstream output;

    void prepareOutput();
};


#endif /* _SCENE_H_ */
