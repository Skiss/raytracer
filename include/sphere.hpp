#ifndef _SPHERE_H_
#define _SPHERE_H_

#include "object.hpp"
#include <glm/glm.hpp>


class Sphere : public Object
{
public:
    Sphere(const glm::vec3& pos, float radius, const glm::vec3& color)
        : position_(pos), radius_(radius), color_(color)
        {}

    ~Sphere() {}

    bool intersect(const Ray& ray, float& dist) override;
    void computeColorNormal(const Ray& ray, float dist,
                            glm::vec3& color, glm::vec3& normal) override;


    glm::vec3 position_;
    float radius_;
    glm::vec3 color_;
};


#endif /* _SPHERE_H_ */
