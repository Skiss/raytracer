#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include <glm/glm.hpp>


class Material
{
public:
    Material(const glm::vec3& color, float reflect, float refraction, float diff, float spec)
        : reflection(reflect), refraction(refraction), diffuse(diff), specular(spec), color(color)
        {}

    virtual glm::vec3 getColor(glm::vec3 coord) const;

    float reflection;
    float refraction;
    float diffuse;
    float specular;

protected:
    glm::vec3 color;
};

class RandomNoiseMat : public Material
{
public:
    RandomNoiseMat(const glm::vec3& color, float reflect, float refract, float diff, float spec)
        : ::Material(color, reflect, refract, diff, spec) {}

    virtual glm::vec3 getColor(glm::vec3 coord) const;
};


class WoodMat : public Material
{
public:
    WoodMat(const glm::vec3& color, float reflect, float refract, float diff, float spec)
        : ::Material(color, reflect, refract, diff, spec) {}

    virtual glm::vec3 getColor(glm::vec3 coord) const;
};


class MarbleMat : public Material
{
public:
    MarbleMat(const glm::vec3& color, float reflect, float refract, float diff, float spec)
        : ::Material(color, reflect, refract, diff, spec) {}

    virtual glm::vec3 getColor(glm::vec3 coord) const;
};


class CheckerMat : public Material
{
public:
    CheckerMat(const glm::vec3& color, float reflect, float refract, float diff, float spec)
        : ::Material(color, reflect, refract, diff, spec) {}

    virtual glm::vec3 getColor(glm::vec3 coord) const;
};


#endif /* _MATERIAL_H_ */
