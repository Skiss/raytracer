#ifndef _PERLIN_H_
#define _PERLIN_H_

double noise(double x, double y, double z);
float noisef(float x, float y, float z);

#endif /* _PERLIN_H_ */
