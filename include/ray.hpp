#ifndef _RAY_H_
#define _RAY_H_

#include <glm/glm.hpp>


struct Ray
{
    Ray(const glm::vec3& orig, const glm::vec3& dir)
        : origin(orig), direction(dir) {}

    glm::vec3 origin;
    glm::vec3 direction;
};


#endif /* _RAY_H_ */
