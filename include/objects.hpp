#ifndef _OBJECTS_H_
#define _OBJECTS_H_

#include "material.hpp"


struct Ray;

struct Object
{
    Object(const glm::vec3& position, Material* m, bool bump = false)
        : position(position), mat(m), bumped(bump)
        {}

    virtual bool hitRay(const Ray& r, float&& t) const = 0;
    virtual glm::vec3 getNormal(const glm::vec3& origin) const = 0;

    glm::vec3 position;
    Material* mat;
    bool bumped;
};

struct Sphere : Object
{
    Sphere(const glm::vec3& pos, float radius, Material* m,
           bool bump, float perlinCoef)
        : ::Object(pos, m, bump), size(radius), perlinCoef(perlinCoef)
        {}

    bool hitRay(const Ray& r, float&& t) const override;
    glm::vec3 getNormal(const glm::vec3& origin) const override;

    float size;
    float perlinCoef;
};

struct Plane : Object
{
    Plane(const glm::vec3& pos, const glm::vec3& normal, Material* m, float D, bool bump = false)
        : ::Object(pos, m, bump), normal(normal), D(D)
        {}

    bool hitRay(const Ray& r, float&& t) const override;
    glm::vec3 getNormal(const glm::vec3& origin) const override;

    mutable glm::vec3 normal;
    float D;
};


#endif /* _OBJECTS_H_ */
