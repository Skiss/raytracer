#include "scene.hpp"
#include "objects.hpp"

#include <cstdlib>
#include <string>
#include <fstream>


void parseSceneFile(Scene& scene, const std::string& fileName)
{
    std::ifstream file(fileName);

    if (!file) std::abort();

    std::string line;
    std::vector<Material*> mats;
    std::size_t posFile = 0;

    while (std::getline(file, line)) {
        if (line.size() > 1 && line.at(0) != '#') {
            std::size_t pos = line.find_first_of(' ');
            std::string comp = line.substr(0, pos);
            std::string vals = line.substr(pos);
            file.seekg(posFile + pos);

            if (comp == "Light") {
                float x, y, z, r, g, b;
                file >> x >> y >> z >> r >> g >> b;
                scene.addLight(Light{glm::vec3(x, y, z), glm::vec3(r, g, b)});
            }
            else if (comp == "Material") {
                float r, g, b, reflect, refract, diff, spec;
                file >> r >> g >> b >> reflect >> refract >> diff >> spec;
                mats.push_back(new Material(glm::vec3(r, g, b), reflect, refract, diff, spec));
            }
            else if (comp == "WoodMat") {
                float r, g, b, reflect, refract, diff, spec;
                file >> r >> g >> b >> reflect >> refract >> diff >> spec;
                mats.push_back(new WoodMat(glm::vec3(r, g, b), reflect, refract, diff, spec));
            }
            else if (comp == "CheckerMat") {
                float r, g, b, reflect, refract, diff, spec;
                file >> r >> g >> b >> reflect >> refract >> diff >> spec;
                mats.push_back(new CheckerMat(glm::vec3(r, g, b), reflect, refract, diff, spec));
            }
            else if (comp == "MarbleMat") {
                float r, g, b, reflect, refract, diff, spec;
                file >> r >> g >> b >> reflect >> refract >> diff >> spec;
                mats.push_back(new MarbleMat(glm::vec3(r, g, b), reflect, refract, diff, spec));
            }
            else if (comp == "RandomNoiseMat") {
                float r, g, b, reflect, refract, diff, spec;
                file >> r >> g >> b >> reflect >> refract >> diff >> spec;
                mats.push_back(new RandomNoiseMat(glm::vec3(r, g, b), reflect, refract, diff, spec));
            }
            else if (comp == "Sphere") {
                float x, y, z, size, mat, perlin;
                bool bump;
                file >> x >> y >> z >> size >> mat
                     >> std::boolalpha >> bump >> perlin;
                scene.addObject(new Sphere(glm::vec3(x, y, z), size, mats.at(mat - 1), bump, perlin));
            }
            else if (comp == "Plane") {
                float x, y, z, normX, normY, normZ, mat;
                file >> x >> y >> z >> normX >> normY >> normZ >> mat;
                scene.addObject(new Plane(glm::vec3(x, y, z), glm::vec3(normX, normY, normZ),
                                          mats.at(mat - 1), 0.F));
            }
        }

        file.seekg(posFile + line.size() + 1);
        posFile += line.size() + 1;
    }
}

int main(int argc, char** argv)
{
    if (argc < 5) std::abort();

    Scene scene(std::stoi(std::string(argv[1])), std::stoi(std::string(argv[2])), argv[4]);

    parseSceneFile(scene, argv[3]);

    scene.render();

    return 0;
}
