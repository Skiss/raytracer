#include "sphere.hpp"
#include "ray.hpp"

#include <cmath>


bool
Sphere::intersect(const Ray& ray, float& dist)
{
    float A = 1.f;
    float B = glm::dot(ray.getDirection(), (ray.getOrigin() - position_));
    float C = glm::length(ray.getOrigin() - position_) - radius_ * radius_;

    float delta = (B * B - A * C);

    if (delta < 0.f)
      return false;

    float disc = std::sqrt(delta);

    if ((dist = -(B + disc)) < 0.f)
        dist = -(B - disc);

    return true;
}

void
Sphere::computeColorNormal(const Ray& ray, float dist,
                           glm::vec3& color, glm::vec3& normal)
{
    glm::vec3 collide(ray.getOrigin() + dist * ray.getDirection());
    normal = collide - position_;
    normal *= 1.f / glm::length(normal);

    color = this->color_;
}
