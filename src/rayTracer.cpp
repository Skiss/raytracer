#include "rayTracer.hpp"

#include "ray.hpp"
#include "object.hpp"
#include "scene.hpp"

#include <algorithm>
#include <cmath>

// #ifndef M_PI
//    #define M_PI 3.14159265358979323846
// #endif

// RayTracer::RayTracer(unsigned pixelWidth, unsigned pixelHeight, float width, float height, float depth)
//     : origin_(0.f, 0.f, 0.f), direction_(0.f, 0.f, 1.f),
//       width_(width), height_(height),
//       pixelW_(pixelWidth), pixelH_(pixelHeight),
//       depth_(depth),
//       precompW_(width_ / pixelW_), precompH_(height_ / pixelH_),
//       output("output.tga")
// {
//     output.put(0).put(0);
//     output.put(2);

//     output.put(0).put(0);
//     output.put(0).put(0);
//     output.put(0);

//     output.put(0).put(0);
//     output.put(0).put(0);

//     output.put((pixelW_ & 0x00FF)).put((pixelW_ & 0xFF00) / 256);
//     output.put((pixelH_ & 0x00FF)).put((pixelH_ & 0xFF00) / 256);
//     output.put(24);
//     output.put(0);
// }

// void
// RayTracer::generateRay(unsigned x, unsigned y, Ray& ray)
// {
//     glm::vec3 direction;
//     float fovx = M_PI / 4.f;
//     float fovy = 1.333333f * fovx;
//     direction.x = 2;
//     direction.y = 2.f * (float)y * ((float)(y / pixelH_) - 0.5f);
//     direction.z = depth_;

//     ray.setDirection(glm::normalize(direction));
// }

// void
// RayTracer::render()
// {
//     Ray ray(origin_, direction_);

//     for (unsigned i = 0; i < pixelW_; ++i) {
//         for (unsigned j = 0; j < pixelH_; ++j) {
//             generateRay(i, j, ray);

//             glm::vec3 resColor;
//             computeColor(ray, resColor);

//             output << (unsigned char)std::min(resColor.z * 255.f, 255.0f)
//                    << (unsigned char)std::min(resColor.y * 255.f, 255.0f)
//                    << (unsigned char)std::min(resColor.x * 255.f, 255.0f);
//         }
//     }
// }

// void
// RayTracer::computeColor(const Ray& ray, glm::vec3& color)
// {
//     float dist = 0.f;
//     Object* obj = scene_->getFirstIntersection(ray, dist);

//     if (obj == nullptr) {
//       return;
//     }


//     glm::vec3 normal;
//     obj->computeColorNormal(ray, dist, color, normal);
// }

// void
// RayTracer::setResolution(unsigned pixelWidth, unsigned pixelHeight)
// {
//     pixelH_ = pixelHeight;
//     pixelW_ = pixelWidth;

//     precompH_ = height_ / pixelH_;
//     precompW_ = width_ / pixelW_;
// }

// void
// RayTracer::setCamera(float width, float height, const glm::vec3& origin, const glm::vec3& direction)
// {
//     width_ = width;
//     height_ = height;
//     origin_ = origin;
//     direction_ = direction;

//     precompH_ = height_ / pixelH_;
//     precompW_ = width_ / pixelW_;
// }
