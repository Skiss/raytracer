#include "material.hpp"
#include "perlin.hpp"

#include <cmath>


glm::vec3 interpolCos(const glm::vec3& C1, const glm::vec3& C2, float coef)
{
    float ft, f;
    ft = coef * 3.1415927;
    f = (1 - std::cos(ft)) * 0.5;

    return  (C1 * (1 - f) + C2 * f);
}

glm::vec3 interpolLineaire(const glm::vec3& C1, const glm::vec3& C2, float coef)
{
    return (C1 * (1 - coef) + C2 * coef);
}

glm::vec3
Material::getColor(glm::vec3 ) const
{
    return color;
}

glm::vec3
WoodMat::getColor(glm::vec3 coord) const
{
    coord *= 0.025f;
    float NoiseCoef;
    glm::vec3 C1 (0.137F, 0.062F, 0.004F),
        C2(0.529F, 0.392F, 0.196F),
        C3(0.62f, 0.32F, 0.17f);
    float v1 = 0.3;
    float v2 = 0.8;
    float v3 = 0.9;

    float v = 20 * noisef(coord.x, coord.y, coord.z);
    NoiseCoef = v - int(v);

    if (NoiseCoef < v1)
        return C1;
    if (v1 < NoiseCoef && NoiseCoef < v2)
        return C1 * ( NoiseCoef - v1)/(v2 - v1)  + C2 * (v2 - NoiseCoef)/(v2 - v1);
    if (v2 < NoiseCoef && NoiseCoef < v3)
        return C2 * ( NoiseCoef - v2)/(v3 - v2)  + C3 * (v3 - NoiseCoef)/(v3 - v2);
    if (NoiseCoef > v3)
        return C2;

    return C2;
}

glm::vec3
MarbleMat::getColor(glm::vec3 coord) const
{
    coord *= 0.50f;
    float NoiseCoef;
    glm::vec3 C1(0.F, 0.39F, 0.F),
        C2(0.f, 1.F, 0.f),
        C3(1.F, 1.F, 0.F);
    float v1 = 0.1;
    float v2 = 0.3;
    float v3 = 0.8;

    NoiseCoef = std::cos(coord.x + noisef(coord.x, coord.y, coord.z));

    if (NoiseCoef < v1)
        return C1;
    if (v1 < NoiseCoef && NoiseCoef < v2)
        return C1 * ( NoiseCoef - v1)/(v2 - v1)  + C2 * (v2 - NoiseCoef)/(v2 - v1);
    if (v2 < NoiseCoef && NoiseCoef < v3)
        return C2 * ( NoiseCoef - v2)/(v3 - v2)  + C3 * (v3 - NoiseCoef)/(v3 - v2);
    if (NoiseCoef > v3)
        return C3;

    return C3;
}

glm::vec3
RandomNoiseMat::getColor(glm::vec3 coord) const
{
    coord *= 0.50f;
    float NoiseCoef;
    glm::vec3 C1(0.F, 0.39F, 0.F),
        C2(0.f, 1.F, 0.f),
        C3(1.F, 1.F, 0.F);
    float v1 = 0.1;
    float v2 = 0.3;
    float v3 = 0.8;

    NoiseCoef = noisef(coord.x, coord.y, coord.z);

    if (NoiseCoef < v1)
        return C1;
    if (v1 < NoiseCoef && NoiseCoef < v2)
        return C1 * ( NoiseCoef - v1)/(v2 - v1)  + C2 * (v2 - NoiseCoef)/(v2 - v1);
    if (v2 < NoiseCoef && NoiseCoef < v3)
        return C2 * ( NoiseCoef - v2)/(v3 - v2)  + C3 * (v3 - NoiseCoef)/(v3 - v2);
    if (NoiseCoef > v3)
        return C3;

    return C3;
}

glm::vec3
CheckerMat::getColor(glm::vec3 coord) const
{
    glm::vec3 res;

    float L = 10.F;
    int p1 = std::floor(coord.x) / L,
        p2 = std::floor(coord.y) / L,
        p3 = std::floor(coord.z) / L;

    res = glm::vec3(0.F) + (glm::vec3(1.F) * (float)((std::abs(p1) +
                                                      std::abs(p2) +
                                                      std::abs(p3)) % 2));

    return res;
}
