#include "objects.hpp"
#include "ray.hpp"
#include "perlin.hpp"

#include <iostream>


inline
std::ostream& operator<<(std::ostream& out, const glm::vec3& v)
{
    out << v.x << "," << v.y << "," << v.z;
    return out;
}

glm::vec3
Plane::getNormal(const glm::vec3& origin) const
{
    if (bumped == false)
        return normal;

    float x, y, z, coefX, coefY, coefZ, e = 1.F;

    coefX = normal.x + origin.x;
    coefY = 5.f * normal.y + 10.f * origin.y;
    coefZ = normal.z + origin.z;

    x = noisef(coefX - e, coefY, coefZ) - noisef(coefX + e, coefY, coefZ);
    y = noisef(coefX, coefY - e, coefZ) - noisef(coefX, coefY + e, coefZ);
    z = noisef(coefX, coefY, coefZ - e) - noisef(coefX, coefY, coefZ + e);

    normal = glm::vec3(normal.x + x, normal.y + y, normal.z + z);

    return normal;
}

glm::vec3
Sphere::getNormal(const glm::vec3& origin) const
{
    glm::vec3 normal = (origin - position) * size;

    if (bumped == false)
        return normal;

    float x, y, z, e = 0.1F;
    normal = glm::normalize(normal);

    x = noisef(perlinCoef * normal.x - e, perlinCoef * normal.y, perlinCoef * normal.z) -
        noisef(perlinCoef * normal.x + e, perlinCoef * normal.y, perlinCoef * normal.z);
    y = noisef(perlinCoef * normal.x, perlinCoef * normal.y - e, perlinCoef * normal.z) -
        noisef(perlinCoef * normal.x, perlinCoef * normal.y + e, perlinCoef * normal.z);
    z = noisef(perlinCoef * normal.x, perlinCoef * normal.y, perlinCoef * normal.z - e) -
        noisef(perlinCoef * normal.x, perlinCoef * normal.y, perlinCoef * normal.z + e);

    glm::vec3 normal2 = glm::vec3(normal.x + x, normal.y + y, normal.z + z);

    return normal2;
}

bool
Sphere::hitRay(const Ray& r, float&& t) const
{
    glm::vec3 dist = position - r.origin;
    float B = glm::dot(r.direction, dist);
    float D = B*B - glm::dot(dist, dist) + size * size;

    if (D < 0.0f)
        return false;

    float DiscSqrt = std::sqrt(D);
    float t0 = B - DiscSqrt;
    float t1 = B + DiscSqrt;
    bool res = false;

    if ((t0 > 0.1f) && (t0 < t))
    {
        t = t0;
        res = true;
    }

    if ((t1 > 0.1f) && (t1 < t))
    {
        t = t1;
        res = true;
    }

    return res;
}

bool
Plane::hitRay(const Ray& r, float&& t) const
{
    float dot = glm::dot(glm::normalize(normal), r.direction);

    if (dot == 0.F)
        return false;

    glm::vec3 toOrigin = r.origin - position;
    float toto = -(glm::dot(glm::normalize(normal), toOrigin) + D) / dot;

    if (toto < 0.1F)
        return false;

    if (toto < t)
        t = toto;
    else
        return false;

    return true;
}
