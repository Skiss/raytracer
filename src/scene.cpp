#include "scene.hpp"
#include "objects.hpp"
#include "ray.hpp"

#include <tbb/blocked_range2d.h>
#include <tbb/parallel_for.h>

#include <cmath>
#include <algorithm>


Scene::Scene(int width, int height, const std::string& file)
    : height_(height), width_(width),
      output(file)
{
    prepareOutput();
}

// We prepare the header for the TGA file
void Scene::prepareOutput()
{
    output.put(0).put(0);
    output.put(2);

    output.put(0).put(0);
    output.put(0).put(0);
    output.put(0);

    output.put(0).put(0);
    output.put(0).put(0);

    output.put((width_ & 0x00FF)).put((width_ & 0xFF00) / 256);
    output.put((height_ & 0x00FF)).put((height_ & 0xFF00) / 256);
    output.put(24);
    output.put(0);
}

void Scene::render()
{
    float pi = 3.14159265F;
    std::vector<glm::vec3> finalImg(height_ * width_);

    tbb::blocked_range2d<unsigned, unsigned> range(0, height_, 0, width_);

    tbb::parallel_for(range, [=, &finalImg](const tbb::blocked_range2d<unsigned, unsigned>& r) {
            for (unsigned y = r.rows().begin(); y < r.rows().end(); ++y) {
                for (unsigned x = r.cols().begin(); x < r.cols().end(); ++x) {

                    std::vector<glm::vec3> pixels;
                    for (float i = y; i < y + 1; i += 0.25f)
                        for (float j = x; j < x + 1; j += 0.25f) {

                            glm::vec3 finalLight;
                            float coef = 1.F;

                            // Creating the ray
                            float focale = width_ / std::tan(pi / 5.F);
                            glm::vec3 origin(0.f, 0.f, 0.f);
                            glm::vec3 dir(j - (float)(width_ / 2.f),
                                          i - (float)(height_ / 2.f),
                                          focale);

                            dir = glm::normalize(dir);
                            dir -= origin;

                            Ray ray(origin, glm::normalize(dir));
                            unsigned reflectLevel = 10;

                            while (reflectLevel-- > 0) {
                                // Closest intersection point
                                float t = 100000.F;
                                Object* obj = nullptr;

                                // We check every object in the scene
                                for (Object* o : objects)
                                    if (o->hitRay(ray, std::move(t)))
                                        obj = o;

                                if (obj != nullptr) {
                                    Material* mat = obj->mat;

                                    if (mat->refraction > 0.F) {
                                        continue;
                                    }

                                    finalLight += ambientLight;

                                    // New ray starting from the intersection point
                                    Ray newStart (glm::vec3(ray.origin + t * ray.direction),
                                                  glm::vec3());

                                    glm::vec3 normal = obj->getNormal(newStart.origin);
                                    glm::vec3 diffuseColor(0.f);
                                    glm::vec3 specularColor(0.f);

                                    // For every lights in the scene...
                                    for (Light light : lights) {
                                        bool shadowed = false;

                                        newStart.direction = light.position - newStart.origin;
                                        Ray lightRay(newStart.origin, glm::normalize(newStart.direction));

                                        // Is there any objects blocking the path to the light source ?
                                        for (Object* o : objects)
                                            if (o->hitRay(lightRay, glm::length(newStart.direction)))
                                                shadowed = true;

                                        if (shadowed == true)
                                            continue;

                                        // Diffuse lighting
                                        float diffuse = glm::dot(glm::normalize(newStart.direction),
                                                                 glm::normalize(normal));

                                        if (diffuse > 0.F)
                                            diffuseColor += light.color * mat->diffuse * diffuse;

                                        // Specular lighting
                                        glm::vec3 reflect = glm::reflect(lightRay.direction,
                                                                         glm::normalize(normal));
                                        float dot = glm::dot(glm::normalize(reflect),
                                                             glm::normalize(ray.direction));
                                        float specular = (dot <= 0.F) ? 0.F : dot;

                                        specular = glm::pow(specular, 16.F);
                                        specularColor += light.color * mat->specular * specular;
                                    }

                                    // Computing final color
                                    finalLight *= (1.f - coef);
                                    glm::vec3 finalLight2 = mat->getColor(newStart.origin) * diffuseColor;
                                    finalLight2 += specularColor;
                                    finalLight += (finalLight2 * coef);

                                    // Reflections
                                    if (mat->reflection == 0.F)
                                        reflectLevel = 0;
                                    else {
                                        coef *= mat->reflection;
                                        ray.origin = newStart.origin;
                                        ray.direction = glm::reflect(ray.direction, glm::normalize(normal));
                                    }
                                }
                            }
                            pixels.push_back(finalLight);
                        }

                    glm::vec3 finalColor;

                    std::for_each(begin(pixels), end(pixels),
                                  [&finalColor](const glm::vec3& v) {finalColor += v;});

                    finalColor /= pixels.size();

                    glm::vec3 pixel(std::min(finalColor.z * 255.f, 255.f),
                                    std::min(finalColor.y * 255.f, 255.f),
                                    std::min(finalColor.x * 255.f, 255.f));
                    finalImg.at(y * width_ + x) = pixel;
                }
            }
        });

        for (auto p : finalImg)
            output << (unsigned char)p.x << (unsigned char)p.y << (unsigned char)p.z;
}
